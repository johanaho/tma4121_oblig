import numpy as np
import matplotlib.pyplot as plt

a = 110
lengde = 50 #mm
tid = 4 #sekunder
noder = 100

dx = lengde / noder
dy = lengde / noder

dt = min(dx**2 / (4 * a), dy**2 / (4 * a))

t_noder = int(tid/dt)

u = np.zeros((noder, noder)) + 20 #Platen starter på 20 grader

#Randkrav

for i in range(noder):
    u[0, i] = 20 + 40 * np.sin(i/7)
    u[-1, i] = 20 + 40 * np.sin(i/32)
    u[i, 0] = 20 + 40 * np.sin(i/7)
    #u[i, -1] = 20 + 40 * np.sin(i/7)

j = round(noder/3)
k = round((noder/3) * 2)

while j < k:
    u[j, -1] = 100
    j += 1

fig, axis = plt.subplots()

pcm = axis.pcolormesh(u, cmap=plt.cm.jet, vmin=0, vmax=100)
plt.colorbar(pcm, ax=axis)

reps = 0

while reps < tid :

    w = u.copy()

    for i in range(1, noder - 1):
        for j in range(1, noder - 1):

            dd_ux = (w[i-1, j] - 2*w[i, j] + w[i+1, j])/dx**2
            dd_uy = (w[i, j-1] - 2*w[i, j] + w[i, j+1])/dy**2

            u[i, j] = dt * a * (dd_ux + dd_uy) + w[i, j]

    reps += dt

    pcm.set_array(u)
    axis.set_title("Temperaturer på platen ved t: {:.3f} [s].".format(reps))
    plt.pause(0.01)

plt.show()